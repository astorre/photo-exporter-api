FROM    alpine:3.6

WORKDIR /var/www/photo-exporter-api/

RUN apk --no-cache add ca-certificates

ADD     bin/photo-exporter-api bin/photo-exporter-api

EXPOSE 8080

CMD     bin/photo-exporter-api
