package main

import (
  "log"
  "github.com/gin-gonic/gin"
  "github.com/astorre88/flickr"
  "github.com/astorre88/flickr/groups"
  "io"
  "net/http"
  "os"
  "strings"
  "encoding/json"
  "database/sql"
  _ "github.com/lib/pq"
  "time"
  "fmt"
)

type Client struct {
  httpClient *http.Client
}

func New() (*Client, error) {
  c := &Client{httpClient: http.DefaultClient}
  return c, nil
}

func Open(name string) (io.Reader, error) {
  c, err := New()
  if err != nil {
    return nil, err
  }
  return c.Open(name)
}

func (c *Client) Open(name string) (io.Reader, error) {
  if strings.HasPrefix(name, "http://") || strings.HasPrefix(name, "https://") {
    resp, err := c.httpClient.Get(name)
    if err != nil {
      return nil, err
    }
    return resp.Body, nil
  }
  return os.Open(name)
}

type Photo struct {
  Url string `json:"url" binding:"required"`
}

type Auth struct {
  Code string `form:"code"`
  Error string `form:"error"`
  ErrorDescription string `form:"error_description"`
}

type Body struct {
  AccessToken string `json:"access_token"`
  ExpiresIn string `json:"expires_in"`
  UserId string `json:"user_id"`
  Error string `json:"error"`
  ErrorDescription string `json:"error_description"`
}

type User struct {
  token string
}

func checkErr(err error) {
  if err != nil {
    log.Fatal(err)
  }
}

func gaveFromDB(db *sql.DB, err error) string {
  user := new(User)
  err = db.QueryRow("SELECT access_token FROM users ORDER BY id DESC LIMIT 1;").Scan(&user.token)
  if err == sql.ErrNoRows {
    return ""
  } else if err != nil {
    log.Fatal(err)
  }
  return user.token
}

func saveToDB(body *Body, db *sql.DB, err error) {
  var lastInsertId int
  err = db.QueryRow("INSERT INTO users(access_token,expires_in,user_id,created) VALUES($1,$2,$3,$4) returning id;", body.AccessToken, body.ExpiresIn, body.UserId, time.Now().Local()).Scan(&lastInsertId)
  checkErr(err)
  fmt.Println("last inserted id =", lastInsertId)
}

func accessTokenRequest(auth *Auth) (*http.Response, error) {
  client := &http.Client{}

  req, err := http.NewRequest("GET", "https://oauth.vk.com/access_token", nil)
  if err != nil {
    log.Print(err)
    return nil, err
  }

  q := req.URL.Query()
  q.Add("client_id", os.Getenv("VK_CLIENT_ID"))
  q.Add("client_secret", os.Getenv("VK_CLIENT_SECRET"))
  q.Add("redirect_uri", os.Getenv("VK_AUTH_REDIRECT_URL"))
  q.Add("code", auth.Code)
  req.URL.RawQuery = q.Encode()

  log.Println(req.URL.String())

  return client.Do(req)
}

func handleTokenFetch(db *sql.DB, err error) gin.HandlerFunc {
  return func(c *gin.Context) {
    // c.Header("Access-Control-Allow-Origin", "*")
    // c.Header("Access-Control-Allow-Headers", "access-control-allow-origin, access-control-allow-headers")

    token := gaveFromDB(db, err)
    c.JSON(200, gin.H{
      "token": token,
    })
  }
}

func handleRedirect(db *sql.DB, err error) gin.HandlerFunc {
  return func(c *gin.Context) {
    var auth Auth
    c.BindQuery(&auth)

    if auth.Error != "" {
      log.Printf("VK redirect error! Error: %s. ErrorDescription: %s.", auth.Error, auth.ErrorDescription)
      c.JSON(500, gin.H{
        "error": auth.Error,
        "description": auth.ErrorDescription,
      })
      return
    }

    resp, err := accessTokenRequest(&auth)
    if err != nil {
      log.Print(err)
      c.JSON(500, gin.H{
        "error": err,
      })
      return
    }

    defer resp.Body.Close()

    var body Body
    json.NewDecoder(resp.Body).Decode(&body)

    log.Println(body)

    if body.AccessToken != "" {
      saveToDB(&body, db, err)
    }

    c.Redirect(301, "https://photo-exporter.herokuapp.com/")
  }
}

func handleImageUpload(client *flickr.FlickrClient) gin.HandlerFunc {
  return func(c *gin.Context) {
    var photo Photo
    c.BindJSON(&photo)

    url := photo.Url

    file, err := os.Create(url[len(url)-15:])
    if err != nil {
      log.Print(err)
      c.JSON(500, gin.H{
        "Create file error": err,
      })
      defer file.Close()
      return
    }
    defer file.Close()

    vk_resp, err := http.Get(url)
    if err != nil {
      log.Print(err)
      c.JSON(500, gin.H{
        "vk_resp error": err,
      })
      defer vk_resp.Body.Close()
      return
    }
    defer vk_resp.Body.Close()

    if _, err := io.Copy(file, vk_resp.Body); err != nil {
      log.Print(err)
      c.JSON(500, gin.H{
        "io.Copy error": err,
      })
      return
    }

    if _, err := file.Seek(0, os.SEEK_SET); err != nil {
      log.Print(err)
      c.JSON(500, gin.H{
        "file.Seek error": err,
      })
      return
    }

    resp, err := flickr.UploadReader(client, file, file.Name(), nil)

    if err != nil {
      c.JSON(500, gin.H{
        "Uploader error": err,
      })
    }

    addResp, err := groups.AddPhoto(client, os.Getenv("GROUP_ID"), resp.ID)

    if err != nil {
      c.JSON(500, gin.H{
        "Photo to group error": err,
      })
    } else {
      c.JSON(200, gin.H{
        "data": addResp,
      })
    }
  }
}

func CORSMiddleware() gin.HandlerFunc {
  return func(c *gin.Context) {
    c.Writer.Header().Set("Content-Type", "application/json")
    c.Writer.Header().Set("Access-Control-Allow-Origin", "https://photo-exporter.herokuapp.com")
    c.Writer.Header().Set("Access-Control-Max-Age", "86400")
    c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, UPDATE")
    c.Writer.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
    c.Writer.Header().Set("Access-Control-Expose-Headers", "Content-Length")
    c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")

    if c.Request.Method == "OPTIONS" {
      fmt.Println("OPTIONS")
      c.AbortWithStatus(200)
    } else {
      c.Next()
    }
  }
}

func main() {
  port := os.Getenv("PORT")
  if port == "" {
    log.Fatal("$PORT must be set")
  }

  db, err := sql.Open("postgres", os.Getenv("HEROKU_POSTGRESQL_JADE_URL"))
  checkErr(err)
  defer db.Close()

  router := gin.Default()

  router.Use(CORSMiddleware())

  client := flickr.NewFlickrClient(os.Getenv("API_KEY"), os.Getenv("API_SECRET"))
  client.OAuthToken = os.Getenv("TOKEN")
  client.OAuthTokenSecret = os.Getenv("TOKEN_SECRET")

  api := router.Group("/api/v1")
  {
    api.GET("/vk_auth_redirect", handleRedirect(db, err))
    api.GET("/vk_token", handleTokenFetch(db, err))
    api.POST("/flickr", handleImageUpload(client))
  }

  router.Run(":" + port)
}
