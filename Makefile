NAME=photo-exporter-api

ENVS=PORT=8080 SOMETHING=test

run: build
	${ENVS} bin/${NAME}

build: check_gb_tool check_vendor clean
	gb build

check_vendor:
	if [ ! -d "vendor/src" ]; then gb vendor restore; fi

check_gb_tool:
	if ! which gb > /dev/null; then echo "Need install gb. Run: go get github.com/constabulary/gb/..."; exit 1; fi

# Cleans our project: deletes binaries
clean:
	rm -f bin/*
